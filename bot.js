//	Dependancies
const Eris = require("eris");
const fs = require("fs");
const config = require("./Partius.json");
const mysql = require("mysql");

//	Bot object
const bot = new Eris.CommandClient(config.token, {}, config.commandOptions);

//	State that I want my user object in the upper most scope
let botAuthor = {};

//	DB object
const db = mysql.createPool(config.connectionData);

//	Begin the log file
const date = new Date();
fs.writeFileSync("log.txt", `(${date.toLocaleString()}): BOT STARTING\n`);

// Command list

//	The full list of files
const files = fs.readdirSync("./commands");

//	Create a loop so I can iterate over my files array
while (files.length != 0) {
	const log = function(logMessage) {
		const fsys = require("fs");
		let time = new Date();
		time = time.toLocaleString();

		fsys.appendFileSync(`${process.cwd()}/log.txt`, `\n(${time}): ${logMessage}`);
	};

	//	Assign the current filename to curFile, and remove it from the file list
	const curFile = files.shift();

	//	If the file is not a specified javascript file
	if (curFile.slice(-3) != ".js") {

		//	Get the list of files from the directory
		const dirFiles = fs.readdirSync(`./commands/${curFile}`);

		//	Register the parent command
		require(`./commands/${curFile}/index.js`).register(bot);
		//	Remove it from the directory files
		dirFiles.splice(dirFiles.findIndex((element) => {return element === "index.js";}), 1);
		//	Log it
		log(`Parent command "${curFile}" has been loaded`);

		//	Add the rest of the files to the files list
		dirFiles.forEach((element) => {files.push(`${curFile}/${element}`);});

		//	Skip the rest of the iteration
		continue;
	}

	//	Require in the command from the directory and filename
	//	(Require gives the object presented by modules.export)
	const command = require(`./commands/${curFile}`);
	//	Use the commands "Register" function to register the command
	command.register(bot);
	//	Log that the command was loaded
	log(`Command '${curFile.slice(0, -3)}' has been loaded.`);
}

//	Event Listeners
//	Bot Listeners
bot.on("ready", () => {
	functions.log("BOT INITIATED\n");
	console.log("Ready!");
	//	Assign botAuthor a user object using an ID held in the config file.
	botAuthor = bot.users.get(config.authorID);
	//	Display the green online signal
	bot.editStatus("online", {
		//	Direct users to the bot's help command
		name: `${bot.commandOptions.prefix}help`,
	});
});

//	When the bot DCs
bot.on("disconnect", () => {
	//	Say it DCed
	functions.log("\nDisconnected.\n");
});

//	Whenever an error occurs in the bot
bot.on("error", err => {
	//	Log the error stack
	functions.log(err.stack);
	// Log it into my private logging server
	functions.errorLog(`${err.name}: ${err.message}`);
});

//	DB Listeners
//	When an error occurs in the database
db.on("error", err => {
	//	Log the error code
	functions.log(err.code);
	//	Then log it into my private logging server
	functions.errorLog(err.code);
});

// Global Functions used
const functions = {

	//	Presents an error embed for the client.
	error(err, msg, date) {
		//	Logs the error to console
		functions.log(err);
		//	Logs the error to the private logging server
		functions.errorLog(err);
		//	Returns with the error embed created to display the error to the user.
		return msg.channel.createMessage({
			embed: {
				title: "Looks like something broke",
				description: "BasiliskHill will attempt to fix this, give him a bit",
				timestamp: date.toISOString(),
				color: 0xd50000,
				footer: functions.footer(msg),
			},
		});
	},

	//	Gets an array from between two quotation marks in the arguments of a command
	getString(args, expectedStart) {
		//	Assigns the last character position (of the arg where the string starts)
		const lastCharPos = args[expectedStart].length - 1;
		//	If the first argument is also the last
		//	(If the quotes close in the same arg where they open)
		if (args[expectedStart].charAt(lastCharPos) == "\"") {
			//	Return an array (for conveniences sake) of the word (removing any quotes)
			//	This ends the function
			return [args.shift().slice(1, lastCharPos)];
		}

		args = args.slice(expectedStart);
		//	Starts the output array with the first arg (removing the quote)
		const output = [args.shift().slice(1)];

		//	Iterates over arguments, assigning the current arrayID to "word"
		for (const word in args) {
			//	If the last character of the arg is a quote
			if (args[word].slice(-1) === "\"") {
				//	Push the arg (without quotes) to the output array
				output.push(args[word].slice(0, -1));
				//	Stop the for loop
				break;
			}
			//	Since there are no quotes push to the output array and continue
			output.push(args[word]);
		}
		//	Return the output array
		return output;

	},

	log: function(logMessage) {
		const fsys = require("fs");
		let time = new Date();
		time = time.toLocaleString();

		fsys.appendFileSync(`${process.cwd()}/log.txt`, `\n(${time}): ${logMessage}`);
	},

	//	Logs any errors into the private logging server
	errorLog: function(err) {
		//	Send a message containing my mention and the error
		bot.createMessage("390790193231167488", `${botAuthor.mention} ${err}`);
	},

	//	Returns an object for the footer of an embed
	footer: function(msg) {
		//	Returns the object
		return {
			text: `Requested by ${msg.author.username}`,
			icon_url: msg.author.avatarURL,
		};
	},

	//	An index function for any parent commands
	index: function(msg, commandObj) {
		//	Set the date for the embed timestamp
		const date = new Date();
		//	Open a semi-global commandList array
		const commandList = [];

		//	Loop over the command's subcommands
		for (const command in commandObj.subcommands) {
			//	Push the subcommand into the command list
			commandList.push(command);
		}

		//	Send the actual message containing the formating and subcommand list
		msg.channel.createMessage({
			embed: {
				title: "This is a parent command.",
				description: `Command list: \n${msg.prefix}${commandObj.label} ${commandList.join(`\n${msg.prefix}${commandObj.label} `)}`,
				timestamp: date.toISOString(),
				color: 0xd50000,
				footer: functions.footer(msg),
			},
		});
	},
};

//	Exports
module.exports = {
	db: db,
	functions: functions,
};

//	Connect the bot to the Discord API
bot.connect();