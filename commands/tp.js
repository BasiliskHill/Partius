module.exports = {
	register: function(bot) {
		bot.registerCommand("tp", (msg, args) => {
			//	Require the database object from bot.js
			const { functions, db } = require(`${process.cwd()}/bot.js`);
			//	Initialise the date object
			const date = new Date();

			//	Get a connection from the database pool
			db.getConnection((err, connection) => {
				//	If the connection is unavailable, log it, tell the user, and exit
				if (connection == undefined) return functions.error("connection unavailable", msg, date);

				//	Query the database to check for the word entered by the user
				connection.query(`SELECT * FROM toki WHERE word = '${args[0]}';`,
					function(error, results) {
						//	If that presents an error, log it, tell the user, and exit
						if (error) return functions.error(error, msg, date);

						//	If the database doesn't have the word
						if (results[0] == undefined) {
							//	Get angry with Leery...
							//	I mean, tell the user
							msg.channel.createMessage({
								embed: {
									title: "It looks like that isn't a word",
									description: `You inputed "${args[0]}" which doesn't seem to be in the dictionary. ` +
										"Maybe try a Toki Pona word next time, might fix your problems.",
									timestamp: date.toISOString(),
									color: 0xd50000,
									footer: functions.footer(msg),
								},
							});
						}
						//	If the database does have the word
						else {
							//	Make a semi-global embedFields array
							const embedFields = [];

							//	Loop over results
							for (const type in results[0]) {
								//	If the type exists and isn't the word label
								if (results[0][type].length > 0 && type != "word") {
									//	Make a field info object
									const fieldInfo = {};

									//	Change the name of the field
									fieldInfo.name = `"${results[0].word}" as a ${type}:`;
									//	and the value of the field
									fieldInfo.value = results[0][type];

									//	Then push that into embedFields
									embedFields.push(fieldInfo);
								}
							}

							//	Use embedFields to send the word definitions to the user
							msg.channel.createMessage({
								embed: {
									title: "The input you gave me matches a word in my dictionary",
									description: `"${args[0]}" matches "${results[0].word}" in the Toki Pona dictionary`,
									timestamp: date.toISOString(),
									color: 0xd50000,
									footer: functions.footer(msg),
									fields: embedFields,
								},
							});
						}
					}
				);

				//	Release the connection back to the pool
				connection.release();
				//	If that presents a problem, log it, and tell the user
				if (err) functions.error(err, msg, date);
			});

		}, {
			aliases: [ "tokiPona", "translate" ],
			usage: "<Toki Pona word>",
			description: "Translates a Toki Pona word into English",
			fullDescription: "Translates Toki Pona words (one at a time) into English. " +
                "Showing the various uses of the word.",
		});
	},
};