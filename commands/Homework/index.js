module.exports = {
	register: function(bot) {
		bot.registerCommand("homework", (msg) => {
			const { functions } = require(`${process.cwd()}/bot.js`);
			//  Use the global function to present the command's subCommand list
			functions.index(msg, bot.commands.homework);

		}, {
			usage: "<subCommand>",
			description: "The parent command for the homework alert system.",
			fullDescription: "The parent command that handles all the homework notification systems.",
			defaultSubcommandOptions: {
				requirements: {
					permissions: {
						"manageGuild": true,
					},
				},
			},
		});
	},
};