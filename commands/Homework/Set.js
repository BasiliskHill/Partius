const register = function(bot) {
	bot.commands.homework.registerSubcommand("set", (msg, args) => {
		//	Functions and database from main file
		const { functions, db } = require(`${process.cwd()}/bot.js`);

		//	Constants
		const editFields = ["subject", "work", "due"];
		const date = new Date();
		const serverID = msg.channel.guild ? msg.channel.guild.id : msg.channel.id;

		//	Variables
		let rowID = 0;
		let changeName = "";
		let changeValue = "";
		let originalRowValue = {};
		let newRowValue = {};

		//	Argument handling
		if (!isNaN(args[0])) {
			rowID = args[0];
		}
		else {
			return msg.channel.createMessage("Please specify a number as the ID");
		}
		if (!editFields.includes(args[1])) {
			return msg.channel.createMessage("Please specify the field to edit");

		}

		//	Database queries
		db.getConnection((err, connection) => {
			if (connection == undefined) return functions.error("connection unavailable", msg, date);

			connection.query(`SELECT * FROM homework WHERE id = ${rowID};`,
				function(err, results) {
					const row = results[0];

					//	If it presents an error, log it, tell the user, and exit
					if (err) return functions.error(err, msg, date);
					//	If the results are nothing, tell the user it doesn't exist and exit
					if (results.length === 0) return msg.channel.createMessage("That isn't a valid id");
					//	If the serverID doesn't match the serverID from the command call, say it isn't theirs
					if (row.serverID != serverID) return msg.channel.createMessage("This isn't from your server is it?");

					originalRowValue = rowValueObject(row.subject, row.work, row.due, row.id);

					switch (args[1].toLowerCase()) {
					case "subject":
						changeName = "subject";
						changeValue = args[2];
						newRowValue = rowValueObject(changeValue, row.work, row.due, row.id);
						break;

					case "work":
						changeName = "work";
						changeValue = functions.getString(args, 2).join(" ");
						newRowValue = rowValueObject(row.subject, changeValue, row.due, row.id);
						break;

					case "due":
						changeName = "due";
						changeValue = `${date.getFullYear().toString()}-${args[2]}`;
						if (new Date(changeValue).toDateString == "Invalid Date") {
							return msg.channel.createMessage("Please enter a valid date (MM-DD)");
						}
						newRowValue = rowValueObject(row.subject, row.work, changeValue, row.id);
						break;
					}

					const timeoutSeconds = 30;

					msg.channel.createMessage({
						embed: {
							title: "Are you sure you wish to make this change?",
							description: "The change won't be permanent, but who wants to redo a change?\n" +
								"\nJust btw, the top is the original info, and the bottom is the new info" +
								`\n\nType \`\`yes\`\` within \`\`${timeoutSeconds}\`\` seconds to ` +
								"make this change\n\nOr type ``no`` to cancel the update",
							timestamp: date.toISOString(),
							color: 0xd50000,
							footer: functions.footer(msg),
							fields: [ originalRowValue, newRowValue ],
						},
					});

					const updateQueryCallback = function(err) {
						//	If it presents an error, log it, tell the user, and exit
						if (err) {
							bot.removeListener("messageCreate", confirmationHandler);
							return functions.error(err, msg, date);
						}
					};

					const confirmationHandler = function(message) {
						if (message.author.id === msg.author.id	&& message.channel.id === msg.channel.id) {
							if (message.content.toLowerCase() === "yes") {
								const updateSQL = `UPDATE homework SET ${changeName} = "${changeValue}" WHERE id = ${rowID};`;
								connection.query(updateSQL, updateQueryCallback(err));
								msg.channel.createMessage(`Sucessfully updated item: ${row.id}.\n` +
								`To make changes to ${changeName}.`);
								bot.removeListener("messageCreate", confirmationHandler);
								return;
							}
							else if (message.content.toLowerCase() === "no") {
								msg.channel.createMessage(`Cancelling update for item: \`\`${row.id}\`\``);
								bot.removeListener("messageCreate", confirmationHandler);
								return;
							}
						}
					};

					bot.on("messageCreate", confirmationHandler);
					setTimeout(() => bot.removeListener("messageCreate", confirmationHandler), timeoutSeconds * 1000);
				}
			);
		});

		const rowValueObject = function(subject, work, due, id) {
			return {
				name: new Date(due).toDateString(),
				value: `\`\`\`ml\nSubject: '${subject}' \nWork Set: "${work}"\n` +
					`ID (used for deletions): ${id}\`\`\`\n`,
			};
		};
	}, {
		aliases: [ "edit", "update" ],
		argsRequired: true,
		description: "A nifty command to edit a listing for an item of homework",
		fullDescription: "\nA command to edit the homework listings as much as you please, " +
			"so long as you use these options: \n**subject** \n**work** \nor **due**",
		usage: "<ID> <option> <change to option>",
	});
};

module.exports = {
	register: register,
};