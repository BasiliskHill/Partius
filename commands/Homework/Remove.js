module.exports = {
	register: function(bot) {
		bot.commands.homework.registerSubcommand("remove", (msg, args) => {
			//	Require the database object from bot.js
			const { db, functions } = require(`${process.cwd()}/bot.js`);
			//	If the first argument isn't a number tell the user they're doing it wrong.
			if (isNaN(Number(args[0]))) return msg.channel.createMessage("Incorrect args passed");

			//	Set the row ID for the DB
			const dbID = args[0];
			//	Set the date
			const date = new Date();
			//	If the command is used in a guild, get the guild id, else get the channel id
			//	(Used for privatising the homework lists)
			const serverID = msg.channel.guild ? msg.channel.guild.id : msg.channel.id;

			//	Get a connection from the database pool
			db.getConnection((err, connection) => {
				//	If the connection is unavailable, log it, tell the user, and exit
				if (connection == undefined) return functions.error("connection unavailable", msg, date);

				//	Query the database for the serverID of the row
				connection.query(`SELECT serverID FROM homework WHERE id = '${dbID}';`,
					function(err, results) {
						//	If it presents an error, log it, tell the user, and exit
						if (err) return functions.error(err, msg, date);
						//	If the results are nothing, tell the user it doesn't exist and exit
						if (results.length === 0) return msg.channel.createMessage("That isn't a valid id");
						//	If the serverID doesn't match the serverID from the command call, say it isn't theirs
						if (results[0].serverID != serverID) return msg.channel.createMessage("This isn't from your server is it?");

						//	If all of that passes, query remove the row from the DB
						connection.query(`DELETE FROM homework WHERE id = '${dbID}';`,
							function(error) {
								//	If that fails, log it, tell the user, and exit
								if (error) return functions.error(error, msg, date);
								//	Else say it succeeded
								return msg.channel.createMessage(`Successfully removed item: ${dbID}`);
							}
						);
					}
				);

				//	Release the connection back to the pool
				connection.release();
				//	If any problems arise, log it, and tell the user
				if (err) functions.error(err, msg, date);
			});

		}, {
			aliases: ["delete"],
			description: "Remove a homework field",
			fullDescription: "Remove a field from the homework table using an id provided in ``homework list``",
			usage: "<id>",
		});
	},
};