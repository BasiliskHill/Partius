module.exports = {
	register: function(bot) {
		bot.commands.homework.registerSubcommand("list", (msg) => {
			//	Require the database object from bot.js
			const { db, functions } = require(`${process.cwd()}/bot.js`);
			//	Initialise the date object
			const date = new Date();
			//	Create a semi-global embedFields array
			const embedFields = [];
			//	If the command is used in a guild, get the guild id, else get the channel id
			//	(Used for privatising the homework lists)
			const serverID = msg.channel.guild ? msg.channel.guild.id : msg.channel.id;

			//	Get a connection from the database pool
			db.getConnection((err, connection) => {
				//	If the connection is unavailable, log it, tell the user, and end the function
				if (connection == undefined) return functions.error("connection unavailable", msg, date);

				//	Query the database to find any homework listed for the server / channel
				connection.query(`SELECT * FROM homework WHERE serverID = ${serverID} ORDER BY due ASC;`,
					function(error, results) {
						//	Initialise a dueDates array
						const dueDates = [];

						//	If an error is encountered in the query, log it, tell the user, and exit
						if (error) return functions.error(error, msg, date);
						//	If nothing is given in results, say there's nothing there and exit.
						if (results.length === 0) return msg.channel.createMessage("You have no homework");

						//	Loop over the results for each row in the database
						for (const row of results) {
							//	Get the dueDate Date object
							let dueDate = new Date(row.due);
							//	Format the due date
							dueDate = `${dueDate.getFullYear()}-` +
							`${dueDate.getMonth() + 1}-${dueDate.getDate()}`;
							//	Set the description of the row
							const description = `\`\`\`ml\nSubject: '${row.subject}' \n` +
								`Work Set: "${row.work}"\n` +
								`ID (used for deletions): ${row.id}\`\`\`\n`;

							//	If the due date of the row hasn't already been accessed
							if (!dueDates.includes(dueDate)) {
								//	Create the output object
								const outputObject = {
									name: `${new Date(dueDate).toDateString()} ( ${dueDate} )`,
									value: description,
								};
								//	Push it to embedFields
								embedFields.push(outputObject);
								//	and say that we've already used the due date
								dueDates.push(dueDate);
							}
							//	If the due date has been used
							else {
								//	Just add the new row description into the same due date object
								embedFields[dueDates.indexOf(dueDate)].value += `${description}`;
							}
						}

						//	Send the list
						msg.channel.createMessage({
							embed: {
								title: "Homework",
								description: "All of the homework added to the bot for this server (ordered by due date)",
								timestamp: date.toISOString(),
								color: 0xd50000,
								footer: functions.footer(msg),
								fields: embedFields,
							},
						});
					});

				//	Release the connection back to the pool
				connection.release();
				//	If there's a problem with that, log it and tell the user.
				if (err) functions.error(err, msg, date);
			});
		}, {
			description: "List homework added",
			fullDescription: "List all of the homework that was added with ``homework add`` for the server",
			requirements: {
				//	Overwrite the default perms
				permissions: {
				},
			},
		});
	},
};