module.exports = {
	register: function(bot) {
		bot.commands.homework.registerSubcommand("add", (msg, args) => {
			//	Require in the database and functions objects from bot.js
			const { functions, db } = require(`${process.cwd()}/bot.js`);
			//	Initiate the date object
			const date = new Date();
			//	Use the first arg as the subject, removing it from the array
			const subject = args.shift();
			//	Call getString and use the output as the work field
			const work = functions.getString(args, 0);
			//	Set the due date to the current year, and use user input for month and day
			const due = `${date.getFullYear().toString()}-${args[work.length]}`;
			//	If the command is used in a guild, get the guild id, else get the channel id
			//	(Used for privatising the homework lists)
			const serverID = msg.channel.guild ? msg.channel.guild.id : msg.channel.id;
			const timeoutSeconds = 30;

			msg.channel.createMessage({
				embed: {
					title: "Are you sure you wish to add this field?",
					description: "While it is entirely possible to remove it again, I don't feel " +
						"like making you do that. \n\nIf you wish to add this field type ``yes`` " +
						`within \`\`${timeoutSeconds}\`\` seconds \n\nOr type \`\`no\`\` to cancel`,
					timestamp: date.toISOString(),
					color: 0xd50000,
					footer: functions.footer(msg),
					fields: [{
						name: `${new Date(due).toDateString()}`,
						value: `\`\`\`ml\nSubject: '${subject}' \n` +
							`Work Set: "${work.join(" ")}"\`\`\`\n`,
					}],
				},
			});

			//	Pull a conncetion from the db pull
			db.getConnection((err, connection) => {
				//	If the connection doesn't exist tell the user and exit.
				if (connection == undefined) return functions.error("connection unavailable", msg, date);

				const confirmationHandler = function(message) {
					if (message.author.id === msg.author.id	&& message.channel.id === msg.channel.id) {
						if (message.content.toLowerCase() === "yes") {
							//	Query the database to insert the values
							connection.query("INSERT INTO homework (subject, work, due, serverID)" +
								`VALUES ('${subject}', "${work.join(" ")}", '${due}', '${serverID}');`),
							function(error) {
								//	If the query spits an error log it and tell the user.
								if (error) return functions.error(error, msg, date);
							};

							bot.removeListener("messageCreate", confirmationHandler);
							msg.channel.createMessage("Successfully added the item");
						}
						else if (message.content.toLowerCase() === "no") {
							msg.channel.createMessage("Cancelling the addition of the item");
							bot.removeListener("messageCreate", confirmationHandler);
						}
					}
				};

				bot.on("messageCreate", confirmationHandler);
				setTimeout(() => bot.removeListener("messageCreate", confirmationHandler), timeoutSeconds * 1000);

				//	Release the connection back to the pool
				connection.release();
				//	If that caused any troubles, log it and tell the user.
				if (err) functions.error(err, msg, date);
			});
		}, {
			description: "Add a field to the homework database",
			fullDescription: "Adds a piece of homework to the database, which can be re-accessed " +
				"with the homework list command",
			usage: "<subject> \"<homework details>\" <MM-DD>",
		});
	},
};