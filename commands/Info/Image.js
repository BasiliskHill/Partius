module.exports = {
	register: function(bot) {
		bot.commands.info.registerSubcommand("image", (msg, args) => {
			//	Create a function named getAvatar that
			const getAvatar = function(user, res) {
				//	Sends the avatar of a user
				msg.channel.createMessage(user.dynamicAvatarURL("png", res));
			};

			//	If there are no mentions
			if (msg.mentions.length === 0) {
				//	and the first arg is a number
				if (!isNaN(args[0])) {
					//	Send the avatar of the author at a user defined resolution
					return getAvatar(msg.author, Number(args[0]));
				}
				//	If the first arg isn't a number, send the avatar of the author at 128
				return getAvatar(msg.author, 128);
			}
			//	If there are mentions
			else{
				// and the second arg is a number
				if (!isNaN(args[1])) {
					//	Return the first mention's avatar at a user defined res
					return getAvatar(msg.mentions[0], Number(args[1]));
				}
				// If the second arg isn't a number send the first mention's avatar at 128
				return getAvatar(msg.mentions[0], 128);
			}
		}, {
			aliases: ["i", "img", "avatar"],
			usage: "[@user] [resolution]",
			guildOnly: true,
			description: "Sends the avatar of a user in a set resolution",
			fullDescription: "**[resolution]** must be any of ``128, 256, 512, 1024, 2048``, " +
				"else the image is posted with a resolution of ``128``\n\n" +
				"Posts the avatar of a specific user (yourself if not specified) " +
				"in a specific resolution (rules listed above).",
			cooldown: 5000,
		});
	},
};