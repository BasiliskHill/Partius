//	Require a URL shortener
const shortUrl = require("node-url-shortener");

module.exports = {
	register: function(bot) {
		bot.registerCommand("info", (msg) => {
			const { functions } = require(`${process.cwd()}/bot.js`);

			// If no one is mentioned
			if (msg.mentions.length === 0) {
				//	Send the author's info and exit
				return userInfo(msg, msg.author, functions);
			}
			//	Send the first mention's info
			userInfo(msg, msg.mentions[0], functions);
		}, {
			aliases: ["information", "in", "mi"],
			usage: "<@user>",
			guildOnly: true,
			description: "WIP User information sender.",
			fullDescription: "WIP command. Will be used to send general information on the user aswell as leaderboard statuses and game stats.",
			cooldown: 5000,
		});
	},
};

function userInfo(msg, user, functions) {
	//	Get the nickname of the user passed
	const nickname = msg.channel.guild.members.get(user.id).nick;
	//	Initialise the date object
	const date = new Date();

	//	Shorten the avatar URL
	shortUrl.short(user.dynamicAvatarURL("png", 2048), function(err, url) {
		//	Use the short URL in the making of the message
		msg.channel.createMessage({
			embed: {
				description: `**This embed displays all information I have on ${user.username}**`,
				timestamp: date.toISOString(),
				color: 0xd50000,
				footer: functions.footer(msg),
				thumbnail: {
					url: user.avatarURL,
				},
				author: {
					name: ((nickname == null) ? user.username : nickname) + "'s Information",
					icon_url: user.avatarURL,
				},
				fields: [
					{
						name: "__**General Information:**__",
						value: "<-=->(+)<-=->",
					},
					{
						name: "Username:",
						value: user.username,
						inline: true,
					},
					{
						name: "Avatar:",
						value: url,
						inline: true,
					},
					{
						name: "__**PUBG Information:**__",
						value: "Additional fields coming soon along with the PUBG commands.",
					},
				],
			},
		});
	});
}
