module.exports = {
	register: function(bot) {
		bot.registerCommand("mod", (msg) => {
			const { functions } = require(`${process.cwd()}/bot.js`);
			//  Use the global function to present the command's subCommand list
			functions.index(msg, bot.commands.mod);

		}, {
			usage: "<subCommand>",
			description: "The parent command for all your moderation needs",
			fullDescription: "An organisational parentCommand containing all of the moderation type commands",
			defaultSubcommandOptions: {
				requirements: {
					permissions: {
						"administrator": true,
					},
				},
			},
		});
	},
};