module.exports = {
	register: function(bot) {
		bot.commands.mod.subcommands.purge.registerSubcommand("all", async (msg) => {
			const { functions } = require(`${process.cwd()}/bot.js`);
			//	Initialises the date object
			const date = new Date();
			//	Starts a variable for messages killed
			let messagesKilled = 0;

			//	Loops over the channel types
			for (const channel of msg.channel.guild.channels.values()) {
				//	If the channel type is 0 (text channel)
				if (channel.type === 0) {
					//	Purge the channel and add the amount of messages killed to the variable
					messagesKilled += await bot.purgeChannel(channel.id, -1);
				}
			}

			// Discord logging
			msg.channel.createMessage({
				embed: {
					title: "Purge ALL completed.",
					description:"Deleted a total of " + (messagesKilled - 1) + " message(s) from the server.",
					timestamp: date.toISOString(),
					color: 0xd50000,
					footer: functions.footer(msg),
				},
			});

		}, {
			guildOnly: true,
			description: "Purge a whole server",
			fullDescription: "Purges all messages < 2 weeks old from **all** channels on the server",
			cooldown: 60000,
			requirements: {
				userIDs: message => [message.channel.guild.ownerID],
			},
		});
	},
};
